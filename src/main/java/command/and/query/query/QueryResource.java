package command.and.query.query;

import command.and.query.command.tab.model.OpenTabCommandRequest;
import command.and.query.domain.tab.TabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Collection;

@Controller
public class QueryResource {

    @Autowired
    private TabService tabService;

    @MessageMapping("/query/{command}/request")
    @SendTo("/query/{command}/response")
    public Collection<OpenTabCommandRequest> query(@DestinationVariable("command") String command) throws Exception {
        Thread.sleep(1000);
        switch (command){
            case "events":
                return tabService.findAll();
            default:
                throw new IllegalArgumentException("command not found");
        }
    }

}