package command.and.query.command.order.model;

public class PlaceOrderCommand {

    private String id;

    private long tableNumber;

    private long waiter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(long tableNumber) {
        this.tableNumber = tableNumber;
    }
}
