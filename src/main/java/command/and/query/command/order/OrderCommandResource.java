package command.and.query.command.order;

import command.and.query.command.common.model.CommandResponse;
import command.and.query.command.common.model.CommandStatus;
import command.and.query.command.order.model.PlaceOrderCommand;
import command.and.query.domain.tab.TabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class OrderCommandResource {

    @Autowired
    private TabService tabService;

    @MessageMapping("/commands/place-order")
    @SendTo("/events/order-placed")
    public CommandResponse command(final PlaceOrderCommand payload) {
        try{
            Thread.sleep(1000); // Delay
            tabService.placeOrder(payload);
            return new CommandResponse(CommandStatus.ACCEPTED);
        }catch(Exception e){
            return new CommandResponse(CommandStatus.REJECTED);
        }
    }

}