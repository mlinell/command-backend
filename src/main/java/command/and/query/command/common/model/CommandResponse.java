package command.and.query.command.common.model;

public class CommandResponse {

    private CommandStatus status;

    public CommandResponse(final CommandStatus status){
        this.status = status;
    }

    public CommandStatus getStatus() {
        return status;
    }

}
