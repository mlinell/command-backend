package command.and.query.command.common.model;


public enum CommandStatus {
    ACCEPTED,
    REJECTED;
}
