package command.and.query.command.tab;

import command.and.query.command.tab.model.OpenTabCommandRequest;
import command.and.query.command.common.model.CommandResponse;
import command.and.query.command.common.model.CommandStatus;
import command.and.query.domain.tab.TabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class TabCommandResource {

    @Autowired
    private TabService tabService;

    @MessageMapping("/commands/tab-open")
    @SendTo("/events/tab-opened")
    public CommandResponse command(final OpenTabCommandRequest payload) {
        try{
            Thread.sleep(1000); // Delay
            tabService.placeOrder(payload);
            return new CommandResponse(CommandStatus.ACCEPTED);
        }catch(Exception e){
            return new CommandResponse(CommandStatus.REJECTED);
        }
    }

    @MessageMapping("/commands/tab-close")
    @SendTo("/events/tab-closed")
    public CommandResponse command(final OpenTabCommandRequest payload) {
        try{
            Thread.sleep(1000); // Delay
            tabService.placeOrder(payload);
            return new CommandResponse(CommandStatus.ACCEPTED);
        }catch(Exception e){
            return new CommandResponse(CommandStatus.REJECTED);
        }
    }

}