package command.and.query.command.menuitem;

import command.and.query.command.common.model.CommandResponse;
import command.and.query.command.common.model.CommandStatus;
import command.and.query.command.menuitem.model.AddMenuItemCommand;
import command.and.query.command.menuitem.model.MenuItemAddedEvent;
import command.and.query.domain.menuitem.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MenuItemCommandResource {

    @Autowired
    private MenuItemService menuItemService;

    @MessageMapping("/commands/add-menu-item")
    @SendTo("/events/menu-item-added")
    public MenuItemAddedEvent addMenuItem(final AddMenuItemCommand payload) {
        try{
            Thread.sleep(1000); // Delay
            menuItemService.addMenuItem(payload);
            return new MenuItemAddedEvent();
        }catch(Exception e){
            return null;
        }
    }

}