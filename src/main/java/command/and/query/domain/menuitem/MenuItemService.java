package command.and.query.domain.menuitem;

import command.and.query.command.menuitem.model.AddMenuItemCommand;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class MenuItemService {

    private static Collection<AddMenuItemCommand> events = new ArrayList<>();

    public void addMenuItem(final AddMenuItemCommand event){
        MenuItemService.events.add(event);
    }

    public Collection<AddMenuItemCommand> findAll(){
        return events;
    }

}
