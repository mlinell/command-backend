package command.and.query.domain.tab;

import command.and.query.command.tab.model.OpenTabCommandRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class TabService {

    private static Collection<OpenTabCommandRequest> events = new ArrayList<>();

    public void placeOrder(final OpenTabCommandRequest event){
        TabService.events.add(event);
    }

    public Collection<OpenTabCommandRequest> findAll(){
        return events;
    }

}
