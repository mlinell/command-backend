package command.and.query.domain.order;

import command.and.query.command.menuitem.model.AddMenuItemCommand;
import command.and.query.command.order.model.PlaceOrderCommand;
import command.and.query.domain.order.model.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class OrderService {

    private static Collection<Order> orders = new ArrayList<>();

    public void addOrder(final PlaceOrderCommand placeOrderCommand){
        final Order order = new Order();
        order.setId(placeOrderCommand.getId());
        order.setItems(placeOrderCommand);
        OrderService.orders.add(placeOrderCommand);
    }

    public Collection<Order> findAll(){
        return orders;
    }

}
