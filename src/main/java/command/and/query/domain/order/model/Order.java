package command.and.query.domain.order.model;

import command.and.query.domain.menuitem.model.MenuItem;

import java.util.Collection;
import java.util.Collections;

public class Order {

    private String id;

    private Collection<MenuItem> items;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<MenuItem> getItems() {
        if (items == null) {
            return Collections.emptyList();
        }
        return items;
    }

    public void setItems(Collection<MenuItem> items) {
        this.items = items;
    }

}
